using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// This script is used to define the behaivour of the 
// popup info that appears whenever a jenga block is clicked.
public class PopupInfoController : MonoBehaviour
{
    [SerializeField] private TMP_Text gradeLabel;
    [SerializeField] private TMP_Text domainLabel;
    [SerializeField] private TMP_Text clusterLabel;
    [SerializeField] private TMP_Text standardIdLabel;
    [SerializeField] private TMP_Text standardDescriptionLabel;

    void Start()
    {
        // Disable camera orbiting
        VirtualCameraController vcc = FindObjectOfType<VirtualCameraController>();
        if (vcc != null)
            vcc.SetEnableOrbit(false);

        // Disable block clicking
        GameController game = FindObjectOfType<GameController>();
        if (game != null)
            game.DisableAllBlocks();

        // Disable the main menu
        MainMenuController mainMenu = FindObjectOfType<MainMenuController>();
        if (mainMenu != null)
            mainMenu.canActivate = false;
    }

    void Update()
    {
        // Close the popup if the escape key is pressed
        if (Input.GetKeyDown(KeyCode.Escape))
            Close();
    }

    public void SetInfo(string gradeText, string domainText, string clusterText, string standardIdText, string standardDescriptionText)
    {
        gradeLabel.text = gradeText;
        domainLabel.text = domainText;
        clusterLabel.text = clusterText;
        standardIdLabel.text = standardIdText;
        standardDescriptionLabel.text = standardDescriptionText;
    }

    // Close the popup
    public void Close()
    {
        // Enable camera obriting
        VirtualCameraController vcc = FindObjectOfType<VirtualCameraController>();
        if (vcc != null)
            vcc.SetEnableOrbit(true);

        // Enable block clicking
        GameController game = FindObjectOfType<GameController>();
        if (game != null)
            game.EnableAllBlocks();

        // Enable the main menu
        MainMenuController mainMenu = FindObjectOfType<MainMenuController>();
        if (mainMenu != null)
            mainMenu.canActivate = true;

        Destroy(gameObject);
    }
}
