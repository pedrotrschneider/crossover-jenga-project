using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private Transform mainMenu;
    [SerializeField] private Transform mainMenuLabel;

    private bool menuActive = false; // Is the menu active?

    public bool canActivate = true; // Can the menu be activated?

    void Start()
    {
        // Disable main menu and enable top-left label
        mainMenu.gameObject.SetActive(false);
        mainMenuLabel.gameObject.SetActive(true);
    }

    void Update()
    {
        // Open and close the menu via the escape key
        if (canActivate && Input.GetKeyDown(KeyCode.Escape))
            if (!menuActive)
                EnableMenu();
            else
                DisableMenu();
    }

    // Open the menu
    public void EnableMenu()
    {
        // Enable main menu and disable the top left label
        mainMenu.gameObject.SetActive(true);
        mainMenuLabel.gameObject.SetActive(false);
        menuActive = true;

        // Disable camera orbiting
        VirtualCameraController vcc = FindObjectOfType<VirtualCameraController>();
        if (vcc != null)
            vcc.SetEnableOrbit(false);

        // Disable block clicking
        GameController game = FindObjectOfType<GameController>();
        if (game != null)
            game.DisableAllBlocks();
    }

    // Close the menu
    public void DisableMenu()
    {
        // Disable main menu and enable top-left label
        mainMenu.gameObject.SetActive(false);
        mainMenuLabel.gameObject.SetActive(true);
        menuActive = false;

        // Enable camera orbiting
        VirtualCameraController vcc = FindObjectOfType<VirtualCameraController>();
        if (vcc != null)
            vcc.SetEnableOrbit(true);

        // Enable block clicking
        GameController game = FindObjectOfType<GameController>();
        if (game != null)
            game.EnableAllBlocks();
    }
}
