using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using UnityEngine;

// This script is used to fetch data from the given API and return it
public class ApiDataFetcher : MonoBehaviour
{
    [Serializable]
    private class Subjects
    {
        public List<SubjectInfo> subjects;
    }

    public List<SubjectInfo> FetchData()
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://ga1vqcu3o1.execute-api.us-east-1.amazonaws.com/Assessment/stack");
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();
        Subjects info = JsonUtility.FromJson<Subjects>("{\"subjects\":" + jsonResponse + "}");
        return info.subjects;
    }
}
