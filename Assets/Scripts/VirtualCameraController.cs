using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

// This script is used to control the virtual camera of the game to enable 
// the orbiting behavior
public class VirtualCameraController : MonoBehaviour
{
    private CinemachineVirtualCamera virtualCamera;
    private CinemachineOrbitalTransposer orbitalTransposer;
    private bool enableOrbit = true;

    void Start()
    {
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
        orbitalTransposer = virtualCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>();
        orbitalTransposer.m_XAxis.m_InputAxisName = ""; // Disable orbiting movement
    }

    void Update()
    {
        if (!enableOrbit) return; // If orbiting is disabled, return out

        if (Input.GetMouseButtonDown(0)) // If the left mouse button has just been pressed
        {
            // Enable orbiting, lock the cursor to the center of the screen and hide it
            orbitalTransposer.m_XAxis.m_InputAxisName = "Mouse X";
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            // Disable block clicking while orbiting
            GameController game = FindObjectOfType<GameController>();
            if (game != null)
                game.DisableAllBlocks();
        }
        else if (Input.GetMouseButtonUp(0)) // If the left mouse button has just been released
        {
            // Disable orbiting, reset the velocity of the movement of the camera, unlock and show the cursor
            orbitalTransposer.m_XAxis.m_InputAxisName = "";
            orbitalTransposer.m_XAxis.m_InputAxisValue = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            // Enable block clicking again
            GameController game = FindObjectOfType<GameController>();
            if (game != null)
                game.EnableAllBlocks();
        }
    }

    public void SetEnableOrbit(bool val)
    {
        enableOrbit = val;
    }
}
