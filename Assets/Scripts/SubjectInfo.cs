using System;
using UnityEngine;

// This class is used to serialize json data from the API
[Serializable]
public class SubjectInfo
{
    [SerializeField]
    private int id;
    [SerializeField]
    private string subject;
    [SerializeField]
    private string grade;
    [SerializeField]
    private int mastery;
    [SerializeField]
    private string domainid;
    [SerializeField]
    private string domain;
    [SerializeField]
    private string cluster;
    [SerializeField]
    private string standardid;
    [SerializeField]
    private string standarddescription;

    public int Id { get { return id; } }
    public string Subject { get { return subject; } }
    public string Grade { get { return grade; } }
    public int Mastery { get { return mastery; } }
    public string DomainId { get { return domainid; } }
    public string Domain { get { return domain; } }
    public string Cluster { get { return cluster; } }
    public string StandardId { get { return standardid; } }
    public string StandardDescription { get { return standarddescription; } }
}
