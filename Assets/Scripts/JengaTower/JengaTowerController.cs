using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

// This script is used to control the behavior of the jenga tower
// after it has been constructed
public class JengaTowerController : MonoBehaviour
{
    [SerializeField] private Transform targetGroup;
    [SerializeField] private CinemachineVirtualCamera virtualCamera;

    // Focus the camera on the tower
    public void Focus()
    {
        virtualCamera.Follow = targetGroup;
        virtualCamera.LookAt = targetGroup;
    }

    // Remove all glass blocks from the tower
    public void RemoveGlass()
    {
        foreach (Transform child in transform)
        {
            SbujectInfoDisplay block = child.GetComponent<SbujectInfoDisplay>();
            if (block != null && block.GetMastery() == 0)
                Destroy(child.gameObject);
        }
    }

    // Reset the tower (that is, remove all blocks currently in the tower and clear block data)
    public void Reset()
    {
        foreach (Transform child in transform)
        {
            SbujectInfoDisplay block = child.GetComponent<SbujectInfoDisplay>();
            if (block != null)
                Destroy(child.gameObject);

            GetComponent<JengaTowerBuilderController>().blockData.Clear();
        }
    }
}
