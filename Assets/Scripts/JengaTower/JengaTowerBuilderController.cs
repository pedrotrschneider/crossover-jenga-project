using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using TMPro;

// This script is used to give a game object the hability to 
// build a jenga tower based on some specified parameters
public class JengaTowerBuilderController : MonoBehaviour
{
    [SerializeField] private Transform jengaBlockPrefab;
    [SerializeField] private bool testBuild = false;
    [SerializeField] private CinemachineTargetGroup targetGroup;
    [SerializeField] private TMP_Text gradeLabel;

    [Header("Block Data")]
    [SerializeField] private Vector3 blockSize;
    [SerializeField] private float blockSeparation;

    [Header("Layer Data")]
    [Range(1, 100)]
    [SerializeField] private int numberOfBlocks;
    [Range(1, 100)]
    [SerializeField] private float layerSeparation;

    private int blocksPerLayer = 3;
    public List<SubjectInfo> blockData;

    void Awake()
    {
        if (!testBuild) // If the tower is not in test mode, set some defaults
            numberOfBlocks = 0;
        else // If it is in test mode, just build the tower
            BuildTower();

        blockData = new List<SubjectInfo>();
    }

    public void BuildTower()
    {
        if (!testBuild) // If not in test mode, number of blocks depends on subject data from the API
            numberOfBlocks = blockData.Count;

        // Order the blocks based on the requested order
        blockData.Sort(delegate (SubjectInfo si1, SubjectInfo si2)
        {
            if (!si1.Domain.Equals(si2.Domain))
                return si1.Domain.CompareTo(si2.Domain);
            if (!si1.Cluster.Equals(si2.Cluster))
                return si1.Cluster.CompareTo(si2.Cluster);
            return si1.StandardId.CompareTo(si2.StandardId);
        });

        for (int i = 0; i < numberOfBlocks; i++)
        {
            int layer = (int)(i / blocksPerLayer); // Calculate the current layer
            int layerId = (layer + 1) % 2; // Calculate the current layer id
            float blockRotation = 90 * layerId; // Calculate the rotation of the current block (0 degrees or 90 degrees)
            float layerHeight = layer * (blockSize.y + layerSeparation) + blockSize.y * 0.5f; // Calculate the current layer height
            int blockId = (i % blocksPerLayer) - (int)(blocksPerLayer * 0.5f); // Calculate the id of the current block
            float blockOffset = (blockSize.x + blockSeparation) * blockId; // Calculate the lateral offset of the current block

            // Calculate the position of the new block
            Vector3 blockPosition = new Vector3((1 - layerId) * blockOffset, layerHeight, layerId * blockOffset);

            // Instantiate the new block
            Transform newBlock = Instantiate(jengaBlockPrefab, Vector3.zero, Quaternion.AngleAxis(blockRotation, Vector3.up), gameObject.transform);
            newBlock.localPosition = blockPosition;

            // Update the new block's size and subject info
            newBlock.GetComponent<JengaBlockController>().SetSize(blockSize);
            if (!testBuild)
                newBlock.GetComponent<SbujectInfoDisplay>().SetInfo(blockData[i]);

            targetGroup.AddMember(newBlock, 1.0f, blockSize.z);
        }
    }

    // Add a block to the tower. All blocks must be added before calling the BuildTower() method
    public void AddBlock(SubjectInfo info)
    {
        blockData.Add(info);
        if (info.Grade.Contains("Grade"))
            gradeLabel.text = info.Grade;
    }
}
