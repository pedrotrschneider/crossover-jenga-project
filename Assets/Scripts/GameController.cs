using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is used to define the main game behavior
public class GameController : MonoBehaviour
{
    [SerializeField] private List<Transform> jengaTowers;

    private ApiDataFetcher fetcher;
    private bool glassRemoved = false;

    void Start()
    {
        Physics.gravity = new Vector3(0, -0.983f, 0);

        // Fetch the data from the API and create the tower
        fetcher = GetComponent<ApiDataFetcher>();
        CreateTowers();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A)) // If [A] is pressed, focus the first tower
            jengaTowers[0].GetComponent<JengaTowerController>().Focus();
        else if (Input.GetKeyDown(KeyCode.S)) // If [S] is pressed, focus the second tower
            jengaTowers[1].GetComponent<JengaTowerController>().Focus();
        else if (Input.GetKeyDown(KeyCode.D)) // If [D] is pressed, focus the thrid tower
            jengaTowers[2].GetComponent<JengaTowerController>().Focus();
        else if (Input.GetKeyDown(KeyCode.F)) // If [F] is pressed, either remove all glass blocks, or reset the simulation
            if (!glassRemoved)
                RemoveAllGlass();
            else
                ResetSimulation();
    }

    private void CreateTowers()
    {
        List<SubjectInfo> subjects = fetcher.FetchData(); // Fetch the data from the API
        foreach (var subject in subjects) // For each entry
        {
            int towerId = subject.Grade switch 
            { // Check what grade it is from
                "6th Grade" => 0,
                "7th Grade" => 1,
                "8th Grade" => 2,
                _ => 0,
            };

            // Add a new block with that subject to the correct tower
            jengaTowers[towerId].GetComponent<JengaTowerBuilderController>().AddBlock(subject);
        }

        // Build all towers
        foreach (var jengaTower in jengaTowers)
            jengaTower.GetComponent<JengaTowerBuilderController>().BuildTower();
    }

    // Enables clicking on all blocks
    public void EnableAllBlocks()
    {
        foreach (var tower in jengaTowers)
        {
            foreach (Transform child in tower)
            {
                JengaBlockController block = child.GetComponent<JengaBlockController>();
                if (block != null)
                    block.SetClickEnabled(true);
            }
        }
    }
    
    // Disables clicking on all blocks
    public void DisableAllBlocks()
    {
        foreach (var tower in jengaTowers)
        {
            foreach (Transform child in tower)
            {
                JengaBlockController block = child.GetComponent<JengaBlockController>();
                if (block != null)
                    block.SetClickEnabled(false);
            }
        }
    }

    // Removes all glass blocks from all towers
    public void RemoveAllGlass()
    {
        glassRemoved = true;
        foreach (var tower in jengaTowers)
            tower.GetComponent<JengaTowerController>().RemoveGlass();
    }

    // Resets the simulation and spawns the towers again
    public void ResetSimulation()
    {
        glassRemoved = false;
        foreach (var tower in jengaTowers)
            tower.GetComponent<JengaTowerController>().Reset();

        CreateTowers();
    }

    // Quit the game
    public void QuitGame()
    {
        Application.Quit();
    }
}
