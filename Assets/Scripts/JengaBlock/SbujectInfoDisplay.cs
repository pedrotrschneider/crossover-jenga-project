using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is used to hold information about the subject 
// related to the jenga block, to be then used to display the popup.
// It is also used to change the material of the block based
// on the mastery of the current subject.
public class SbujectInfoDisplay : MonoBehaviour
{
    [SerializeField] private List<Material> materials;
    [SerializeField] private MeshRenderer visual;

    private SubjectInfo info;

    public void SetInfo(SubjectInfo newInfo)
    {
        info = newInfo;
        visual.material = materials[info.Mastery];
    }

    public int GetMastery()
    {
        return info.Mastery;
    }

    public SubjectInfo GetInfo()
    {
        return info;
    }
}
