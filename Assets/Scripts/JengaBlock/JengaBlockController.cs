using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JengaBlockController : MonoBehaviour
{
    [SerializeField] private Transform visual;
    [SerializeField] private Transform uiPopupPrefab;

    private BoxCollider boxCollider;
    private MeshRenderer visualMeshRenderer;
    private bool mouseOver = false;
    private float transitionSpeed = 10f;
    private bool clickEnabled = true;

    void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        visualMeshRenderer = visual.GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if (mouseOver && clickEnabled) // If the mouse is over the block, change color
        {
            visualMeshRenderer.materials[1].color = Color.Lerp(visualMeshRenderer.materials[1].color, Color.green, transitionSpeed * Time.deltaTime);
            if (Input.GetMouseButtonDown(1)) // If the right mouse button is pressed while on top of the block
            {
                // Focus the current tower
                gameObject.GetComponentInParent<JengaTowerController>().Focus();
                // Open a new info popup
                Transform newPopup = Instantiate(uiPopupPrefab, Vector3.zero, Quaternion.Euler(0, 0, 0));
                // Update the info with the subject info in the controller
                SubjectInfo info = gameObject.GetComponent<SbujectInfoDisplay>().GetInfo();
                newPopup.GetComponent<PopupInfoController>().SetInfo(info.Grade, info.Domain, info.Cluster, info.StandardId, info.StandardDescription);
            }
        }
        else // If not, return to the normal color
            visualMeshRenderer.materials[1].color = Color.Lerp(visualMeshRenderer.materials[1].color, new Color(0, 0, 0, 0), transitionSpeed * Time.deltaTime);

    }

    void OnMouseEnter()
    {
        mouseOver = true;
    }

    void OnMouseExit()
    {
        mouseOver = false;
    }

    public void SetSize(Vector3 newSize)
    {
        boxCollider.size = newSize;
        visual.transform.localScale = newSize;
    }

    public void SetClickEnabled(bool val)
    {
        clickEnabled = val;
    }

}
